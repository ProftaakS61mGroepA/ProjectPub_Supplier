package Bean;

import Models.Order;
import Service.OrderService;

import javax.inject.*;
import javax.enterprise.context.RequestScoped;
import java.io.Serializable;
import java.util.Collection;

@Named(value = "orderBean")
@RequestScoped
public class OrderBean implements Serializable {

    @Inject
    private OrderService service;

    public Collection<Order> getOrders() {
        return service.getAllOrders();
    }

    public void sendOrder(long orderId) {
        service.sendOrder(orderId);
    }
}
