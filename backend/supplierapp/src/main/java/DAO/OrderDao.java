package DAO;

import Models.Order;

import javax.ejb.Stateless;
import java.util.*;

@Stateless
public class OrderDao {

    private final Map<Long, Order> orders = new HashMap<>();

    private long id = 0;

    public Order addOrder(Order order) {
        order.setId(nextId());
        orders.put(order.getId(), order);
        return order;
    }

    public void removeOrder(long id) {
        orders.remove(id);
    }

    public Collection<Order> getAllOrders() {
        return orders.values();
    }

    public Order findById(long id) {
        return orders.get(id);
    }

    private long nextId() {
        id++;
        return id;
    }
}
