package Service;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import java.io.*;
import java.net.URL;
import java.util.Properties;

@Singleton
@Startup
public class ConfigService {

    private final String DEFAULT_CONFIG_FILE = "/config-default.properties";
    private final String PROJECT_PUB_ENV = "PROJECT_PUB_ENV";

    private Properties prop = new Properties();

    @PostConstruct
    private void load() {
        // First load defaults, then let environment-specific overwrite
        try (InputStream defaultInput = this.getClass().getClassLoader().getResourceAsStream(DEFAULT_CONFIG_FILE)) {
            prop.load(defaultInput);
        } catch (IOException ex) { }

        //If environment-specific config file doesn't exist the default properties are still loaded.
        URL configFile = this.getClass().getClassLoader().getResource("config-" + System.getenv(PROJECT_PUB_ENV) + ".properties");
        if (configFile != null) {
            try (InputStream input = configFile.openStream()) {
                prop.load(input);
            } catch (IOException e) {
            }
        }

        System.out.println();
    }

    public String getProperty(String key) {
        return prop.getProperty(key);
    }
}