package Service;

import DAO.OrderDao;
import JMS.JMSBrokerGateway;
import Models.Order;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Collection;

@Stateless
public class OrderService {

    @Inject
    private OrderDao dao;
    @Inject
    private JMSBrokerGateway jmsGateway;

    public OrderService() { }

    public Collection<Order> getAllOrders() {
        return dao.getAllOrders();
    }

    public void onOrderArrived(Order order) {
        dao.addOrder(order);
    }

    public void sendOrder(long id) {
        Order order = dao.findById(id);
        jmsGateway.sendMessage(order.getProductId(), order.getProductName(), order.getAmount());
        dao.removeOrder(order.getId());
    }
}
