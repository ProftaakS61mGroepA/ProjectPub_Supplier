package DAO;

import Models.Order;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class OrderDaoTest {

    @InjectMocks
    private OrderDao dao;

    @Test
    public void addOrderTest() throws Exception {
        Order order = new Order(1, "Cola", 100);
        dao.addOrder(order);
        Assert.assertEquals("Wrong ID was set.",
                true, (order.getId() > 0));
    }

    @Test
    public void getAllOrdersTest() throws Exception {
        dao.addOrder(new Order(1, "Cola", 100));
        dao.addOrder(new Order(2, "Fanta", 100));
        dao.addOrder(new Order(3, "Sprite", 100));
        Assert.assertEquals("Dao did not return the right amount of orders.",
                dao.getAllOrders().size(), 3);
    }

    @Test
    public void removeOrderTest() throws Exception {
        Order orderToRemove = new Order(1, "Cola", 100);
        dao.addOrder(orderToRemove);
        dao.addOrder(new Order(2, "Fanta", 100));
        dao.addOrder(new Order(3, "Sprite", 100));
        dao.removeOrder(orderToRemove.getId());
        Assert.assertEquals("Dao did not return the right amount of orders.",
                dao.getAllOrders().size(), 2);
    }

    @Test
    public void findByIdTest() throws Exception {
        Order order = dao.addOrder(new Order(1, "Cola", 100));
        Assert.assertEquals("Dao did not return the right orders.",
                order, dao.findById(order.getId()));
    }
}
