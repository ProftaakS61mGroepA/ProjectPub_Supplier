package Service;

import DAO.OrderDao;
import JMS.JMSBrokerGateway;
import Models.Order;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collection;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {

    @InjectMocks
    private OrderService service;

    @Spy
    private OrderDao dao;
    @Mock
    private JMSBrokerGateway jmsGateway;

    @Test
    public void onOrderArrivedTest() throws Exception {
        Order order = new Order(1, "Cola", 100);
        service.onOrderArrived(order);
        Mockito.verify(dao).addOrder(order);
    }

    @Test
    public void getAllOrdersTest() throws Exception {
        Collection<Order> orders = new ArrayList<Order>() {{
            add(new Order(1, "Cola", 100));
            add(new Order(2, "Fanta", 100));
            add(new Order(3, "Sprite", 100));
        }};
        long id = 0;
        for (Order o : orders) {
            o.setId(id++);
        }

        Mockito.when(dao.getAllOrders())
                .thenReturn(orders);

        Assert.assertEquals("Service did not return the right amount of orders",
                orders.size(), service.getAllOrders().size());
    }

    @Test
    public void sendOrderTest() throws Exception {
        final long ID = 3153215;
        final Order order = new Order(ID, "Cola", 100);
        order.setId(1);
        Mockito.when(dao.findById(ID))
                .thenReturn(order);
        Mockito.doNothing()
                .when(jmsGateway).sendMessage(Mockito.anyLong(), Mockito.anyString(), Mockito.anyLong());
        service.sendOrder(ID);
        Mockito.verify(dao).findById(ID);
        Mockito.verify(jmsGateway).sendMessage(ID, order.getProductName(), order.getAmount());
    }
}
