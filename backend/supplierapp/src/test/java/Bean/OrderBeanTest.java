package Bean;

import Models.Order;
import Service.OrderService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collection;

@RunWith(MockitoJUnitRunner.class)
public class OrderBeanTest {

    @InjectMocks
    private OrderBean orderBean;
    @Mock
    private OrderService service;

    @Test
    public void getOrdersTest() throws Exception {
        Collection<Order> orders = new ArrayList<Order>() {{
            add(new Order(1, "Cola", 100));
            add(new Order(2, "Fanta", 100));
            add(new Order(3, "Sprite", 100));
        }};
        Mockito.when(service.getAllOrders())
                .thenReturn(orders);

        Assert.assertEquals("Bean did not return the right amount of products.",
            orders.size(), orderBean.getOrders().size());
    }

    @Test
    public void sendOrderTest() throws Exception {
        Mockito.doNothing()
                .when(service)
                .sendOrder(Mockito.anyLong());

        final long ID = 151234;
        orderBean.sendOrder(ID);
        Mockito.verify(service)
            .sendOrder(ID);
    }
}
